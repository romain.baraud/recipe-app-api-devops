resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-files-backend"
  acl           = "private"
  force_destroy = true
}